FROM node:alpine

WORKDIR /service

COPY . .

RUN npm install
RUN npm test

ENV NODE_ENV production
CMD [ "node", "src/server.js" ]

EXPOSE 8082