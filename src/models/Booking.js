import mongoose from "mongoose";

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

export const BookingSchema = new Schema({
  sessionId: String,
  attendees: [String],
});

export const Booking = mongoose.model("booking", BookingSchema);
