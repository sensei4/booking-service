import * as mockingoose from "mockingoose";
import { Booking } from "../models/Booking";
import server from "../server";
import request from "supertest";
import jwt from "jsonwebtoken";

describe("Test routes", () => {
  const OG_ENV = process.env;

  beforeEach(() => {
    jest
      .spyOn(jwt, "verify")
      .mockImplementation(jest.fn((token, secret, callback) => callback(null)));
  });

  beforeAll(() => {
    process.env = {
      ...OG_ENV,
      AUTH_SHARED_SECRET:
        "244226452948404D635166546A576E5A7234753778214125442A462D4A614E64",
      MONGODB_HOST: "localhost",
      MONGODB_USERNAME: "admin",
      MONGODB_PASSWORD: "pass",
    };
  });

  afterAll(async () => {
    process.env = OG_ENV;
    await server.close();
  });

  describe("GET /liveness", () => {
    test("should return 200 response", async () => {
      await request(server).get("/liveness").expect(200);
    });
  });

  describe("GET /api/booking/:sessionId", () => {
    test("should return 200 response when session present in DB", async () => {
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad",
          attendees: ["643691874a09623b431482ad"],
        },
        "findOne"
      );

      const response = await request(server)
        .get("/api/booking/643691874a09623b431482ad")
        .set({ Authorization: "Bearer abc.def.xyz" });
      expect(response.statusCode).toEqual(200);
      expect(response.headers["content-type"]).toEqual(
        expect.stringContaining("json")
      );
      expect(response.body.sessionId).toBeDefined();
      expect(response.body.attendees).toBeDefined();
    });

    test("should return 200 response when session present in DB with date in ID", async () => {
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad:2020-01-01",
          attendees: ["643691874a09623b431482ad"],
        },
        "findOne"
      );

      const response = await request(server)
        .get("/api/booking/643691874a09623b431482ad:2020-01-01")
        .set({ Authorization: "Bearer abc.def.xyz" });
      expect(response.statusCode).toEqual(200);
      expect(response.headers["content-type"]).toEqual(
        expect.stringContaining("json")
      );
      expect(response.body.sessionId).toBeDefined();
      expect(response.body.attendees).toBeDefined();
    });

    test("should return 404 response when session not present in DB", async () => {
      mockingoose(Booking).toReturn(undefined, "findOne");

      const response = await request(server)
        .get("/api/booking/643691874a09623b431482ad")
        .set({ Authorization: "Bearer abc.def.xyz" });
      expect(response.statusCode).toEqual(404);
    });

    test("should return 404 response when requested sessionId is not valid", async () => {
      const response = await request(server)
        .get("/api/booking/1")
        .set({ Authorization: "Bearer abc.def.xyz" });
      expect(response.statusCode).toEqual(404);
    });

    test("should return 401 response when auth header missing", async () => {
      const response = await request(server).get("/api/booking/1");
      expect(response.statusCode).toEqual(401);
    });

    test("should return 401 response when auth header misfomatted", async () => {
      const response = await request(server)
        .get("/api/booking/1")
        .set({ Authorization: "abc.def.xyz" });

      expect(response.statusCode).toEqual(401);
    });

    test("should return 403 response when token is invalid", async () => {
      jest
        .spyOn(jwt, "verify")
        .mockImplementation(jest.fn((token, secret, callback) => callback({})));

      const response = await request(server)
        .get("/api/booking/1")
        .set({ Authorization: "Bearer abc.def.xyz" });

      expect(response.statusCode).toEqual(403);
    });
  });

  describe("POST /api/booking", () => {
    test("should return 201 response when session present and attendee added", async () => {
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad",
          attendees: ["643691874a09623b431482ad"],
        },
        "findOne"
      );
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad",
          attendees: ["643691874a09623b431482a1", "643691874a09623b431482ad"],
        },
        "save"
      );

      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });
      expect(response.statusCode).toEqual(201);
      expect(response.headers["content-type"]).toEqual(
        expect.stringContaining("json")
      );
      expect(response.body.sessionId).toBeDefined();
      expect(response.body.attendees).toBeDefined();
      expect(response.body.attendees.length).toEqual(2);
    });

    test("should return 201 response when new session created", async () => {
      mockingoose(Booking).toReturn(undefined, "findOne");
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad",
          attendees: ["643691874a09623b431482a1"],
        },
        "save"
      );

      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });
      expect(response.statusCode).toEqual(201);
      expect(response.headers["content-type"]).toEqual(
        expect.stringContaining("json")
      );

      expect(response.body.sessionId).toBeDefined();
      expect(response.body.attendees).toBeDefined();
      expect(response.body.attendees.length).toEqual(1);
    });

    test("should return 201 response when new session created with date in ID", async () => {
      mockingoose(Booking).toReturn(undefined, "findOne");
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad:2023-01-01",
          attendees: ["643691874a09623b431482a1"],
        },
        "save"
      );

      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(201);
      expect(response.headers["content-type"]).toEqual(
        expect.stringContaining("json")
      );

      expect(response.body.sessionId).toBeDefined();
      expect(response.body.attendees).toBeDefined();
      expect(response.body.attendees.length).toEqual(1);
    });

    test("should return 409 response when user already booked into session", async () => {
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad",
          attendees: ["643691874a09623b431482a1"],
        },
        "findOne"
      );

      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(409);
    });

    test("should return 400 response when sessionId not present", async () => {
      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(400);
    });

    test("should return 400 response when userId not present", async () => {
      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(400);
    });

    test("should return 401 response when auth header missing", async () => {
      const response = await request(server).post("/api/booking").send({
        sessionId: "643691874a09623b431482ad",
        userId: "643691874a09623b431482a1",
      });
      expect(response.statusCode).toEqual(401);
    });

    test("should return 401 response when auth header misfomatted", async () => {
      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(401);
    });

    test("should return 403 response when token is invalid", async () => {
      jest
        .spyOn(jwt, "verify")
        .mockImplementation(jest.fn((token, secret, callback) => callback({})));

      const response = await request(server)
        .post("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(403);
    });
  });

  describe("DELETE /api/booking", () => {
    test("should return 204 response when session present and attendee removed", async () => {
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad",
          attendees: ["643691874a09623b431482a1"],
        },
        "findOne"
      );
      mockingoose(Booking).toReturn(
        {
          sessionId: "643691874a09623b431482ad",
          attendees: [],
        },
        "save"
      );

      const response = await request(server)
        .delete("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(204);
    });

    test("should return 204 response when booking not found", async () => {
      mockingoose(Booking).toReturn(undefined, "findOne");

      const response = await request(server)
        .delete("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(204);
    });

    test("should return 400 response when sessionId not present", async () => {
      const response = await request(server)
        .delete("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(400);
    });

    test("should return 400 response when userId not present", async () => {
      const response = await request(server)
        .delete("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(400);
    });

    test("should return 401 response when auth header missing", async () => {
      const response = await request(server).delete("/api/booking").send({
        sessionId: "643691874a09623b431482ad",
        userId: "643691874a09623b431482a1",
      });
      expect(response.statusCode).toEqual(401);
    });

    test("should return 401 response when auth header misfomatted", async () => {
      const response = await request(server)
        .delete("/api/booking")
        .set({ Authorization: "abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(401);
    });

    test("should return 403 response when token is invalid", async () => {
      jest
        .spyOn(jwt, "verify")
        .mockImplementation(jest.fn((token, secret, callback) => callback({})));

      const response = await request(server)
        .delete("/api/booking")
        .set({ Authorization: "Bearer abc.def.xyz" })
        .send({
          sessionId: "643691874a09623b431482ad",
          userId: "643691874a09623b431482a1",
        });

      expect(response.statusCode).toEqual(403);
    });
  });
});
