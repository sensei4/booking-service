import mongoose from "mongoose";
import { Booking } from "./models/Booking.js";
import express from "express";
import cors from "cors";
import jwt from "jsonwebtoken";

const port = 8082;
const app = express();
app.use(express.json());
app.use(cors());

mongoose.connection.once("open", function () {
  console.log("MongoDB database connection established");
});

const authenticateJWT = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader && authHeader.startsWith("Bearer ")) {
    const token = authHeader.split(" ")[1];

    jwt.verify(
      token,
      Buffer.from(process.env.AUTH_SHARED_SECRET, "base64"),
      (err) => {
        if (err) {
          console.log("JWT verification failed, returning 403");
          return res.sendStatus(403);
        }

        next();
      }
    );
  } else {
    console.log("JWT not presented correctly, returning 401");
    res.sendStatus(401);
  }
};

app.get("/liveness", async (req, res) => {
  res.sendStatus(200);
});

app.get("/api/booking/:sessionId", authenticateJWT, async (req, res) => {
  const requestedId = req.params.sessionId;
  const result = await Booking.findOne({
    sessionId: requestedId,
  }).exec();

  if (result) {
    res.status(200).send(result);
  } else {
    res.sendStatus(404);
  }
});

app.post("/api/booking/", authenticateJWT, async (req, res) => {
  const { sessionId, userId } = req.body;

  if (sessionId && userId) {
    const result = await Booking.findOne({
      sessionId: sessionId,
    }).exec();

    if (result) {
      if (result.attendees.includes(userId)) {
        res.sendStatus(409);
      } else {
        console.log(`Adding attendee (${userId}) to session (${sessionId})`);
        result.attendees.push(userId);
        result.save();
        res.status(201).send(result);
      }
    } else {
      console.log(`Creating new session (${sessionId}), attendee (${userId})`);
      const newRecord = new Booking({
        sessionId: sessionId,
        attendees: [userId],
      });
      await newRecord.save();

      res.status(201).send(newRecord);
    }
  } else {
    res.sendStatus(400);
  }
});

app.delete("/api/booking/", authenticateJWT, async (req, res) => {
  const { sessionId, userId } = req.body;

  if (sessionId && userId) {
    console.log(
      `Removing booking - session: ${sessionId}, attendee: ${userId}`
    );

    const result = await Booking.findOne({
      sessionId: sessionId,
    }).exec();

    if (result) {
      result.attendees = result.attendees.filter(
        (attendee) => attendee !== userId
      );

      await result.save();
      res.sendStatus(204);
    } else {
      res.sendStatus(204);
    }
  } else {
    res.sendStatus(400);
  }
});

const server = app.listen(port, async () => {
  if (
    !process.env.AUTH_SHARED_SECRET ||
    !process.env.MONGODB_HOST ||
    !process.env.MONGODB_USERNAME ||
    !process.env.MONGODB_PASSWORD
  )
    throw new Error("Required ENV variables not present.");

  if (process.env.NODE_ENV != "test")
    await mongoose.connect(
      `mongodb://${process.env.MONGODB_USERNAME}:${
        process.env.MONGODB_PASSWORD
      }@${process.env.MONGODB_HOST}:${
        process.env.MONGODB_PORT || 27017
      }/booking-service?authSource=admin`
    );

  console.log(`Listening on port ${port}`);
});

export default server;
